<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <title>ZalCG computational performance | Xyst docs</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i%7CSource+Code+Pro:400,400i,600" />
  <link rel="stylesheet" href="xyst.m-dark-noindent+doxygen.compiled.css" />
  <link rel="icon" href="Eo_circle_deep-orange_letter-x.svg" type="image/svg+xml" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="theme-color" content="#22272e" />
</head>
<body>
<header><nav id="navigation">
  <div class="m-container">
    <div class="m-row">
      <a href="index.html" id="m-navbar-brand" class="m-col-t-8 m-col-m-none m-left-m">Xyst <span class="m-thin">docs</span></a>
      <div class="m-col-t-4 m-hide-m m-text-right m-nopadr">
        <a href="#search" class="m-doc-search-icon" title="Search" onclick="return showSearch()"><svg style="height: 0.9rem;" viewBox="0 0 16 16">
          <path id="m-doc-search-icon-path" d="m6 0c-3.31 0-6 2.69-6 6 0 3.31 2.69 6 6 6 1.49 0 2.85-0.541 3.89-1.44-0.0164 0.338 0.147 0.759 0.5 1.15l3.22 3.79c0.552 0.614 1.45 0.665 2 0.115 0.55-0.55 0.499-1.45-0.115-2l-3.79-3.22c-0.392-0.353-0.812-0.515-1.15-0.5 0.895-1.05 1.44-2.41 1.44-3.89 0-3.31-2.69-6-6-6zm0 1.56a4.44 4.44 0 0 1 4.44 4.44 4.44 4.44 0 0 1-4.44 4.44 4.44 4.44 0 0 1-4.44-4.44 4.44 4.44 0 0 1 4.44-4.44z"/>
        </svg></a>
        <a id="m-navbar-show" href="#navigation" title="Show navigation"></a>
        <a id="m-navbar-hide" href="#" title="Hide navigation"></a>
      </div>
      <div id="m-navbar-collapse" class="m-col-t-12 m-show-m m-col-m-none m-right-m">
        <div class="m-row">
          <ol class="m-col-t-12 m-col-m-none">
            <li>
              <a href="index.html#mainpage_tools">Tools</a>
              <ol>
                <li><a href="inciter_main.html">Inciter</a></li>
                <li><a href="meshconv_main.html">MeshConv</a></li>
                <li><a href="unittest_main.html">UnitTest</a></li>
              </ol>
            </li>
            <li>
              <a href="vnv.html">V&amp;V</a>
              <ol>
                <li><a href="vnv.html#riecg_vnv">RieCG</a></li>
                <li><a href="vnv.html#kozcg_vnv">KozCG</a></li>
                <li><a href="vnv.html#zalcg_vnv">ZalCG</a></li>
                <li><a href="vnv.html#laxcg_vnv">LaxCG</a></li>
                <li><a href="vnv.html#chocg_vnv">ChoCG</a></li>
                <li><a href="vnv.html#lohcg_vnv">LohCG</a></li>
              </ol>
            </li>
            <li><a href="howtos.html">Howtos</a></li>
            <li><a href="papers.html">Papers</a></li>
            <li>
              <a href="https://codeberg.org/xyst/xyst">Source</a>
              <ol>
                <li><a href="namespaces.html">Namespaces</a></li>
                <li><a href="annotated.html">Classes</a></li>
                <li><a href="files.html">Files</a></li>
                <li><a href="licenses.html">Libraries</a></li>
                <li><a href="coverage.html">Quality</a></li>
                <li><a href="contributing.html">Contributing</a></li>
              </ol>
            </li>
          </ol>
          <ol class="m-col-t-6 m-col-m-none" start="6">
            <li class="m-show-m"><a href="#search" class="m-doc-search-icon" title="Search" onclick="return showSearch()"><svg style="height: 0.9rem;" viewBox="0 0 16 16">
              <use href="#m-doc-search-icon-path" />
            </svg></a></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</nav></header>
<main><article>
  <div class="m-container m-container-inflatable">
    <div class="m-row">
      <div class="m-col-l-10 m-push-l-1">
        <h1>
          ZalCG computational performance
        </h1>
<p>This page discusses the computational performance of the <a href="inciter_zalcg.html" class="m-doc">ZalCG</a> solver. The timings demonstrate that there is no significant scalability bottleneck in computational performance.</p><section id="zalcg_strong_scaling"><h2><a href="#zalcg_strong_scaling">Strong scaling of computation</a></h2><p>Using increasing number of compute cores with the same problem measures <em>strong scaling</em>, characteristic of the algorithm and its parallel implementation. Strong scalability helps answer questions, such as <em>How much faster one can obtain a given result (at a given level of numerical error) if larger computational resources were available</em>. To measure strong scaling we ran the <a href="riecg_example_taylor_green.html" class="m-doc">Taylor-Green problem</a> using a 794M-cell and a 144M-cell mesh on varying number of CPUs for a few time steps and measured the average wall-clock time it takes to advance a single time step. The figure below depicts timings measured for both meshes on the <a href="https://www.lumi-supercomputer.eu/lumi_supercomputer">LUMI</a> computer.</p><div class="m-col-m-10 m-center-m"><img class="m-image" src="images/xyst_zalcg_strong.png" alt="Image" /></div><p>The figure shows that the <a href="inciter_zalcg.html" class="m-doc">ZalCG</a> solver, while not ideal for all runs, scales well into the range of O(10^4) CPU cores. In particular, the figure shows that strong scaling is ideal at and below 1024 CPUs using the smaller work-load of 144M cells and for the larger mesh at and below approximately 8912 CPUs. The departure from ideal is indicated by nonzero angles between the ideal and the blue lines. The data also shows that though non-ideal above these points, parallelism is still effective in reducing CPU time with increasing compute resources for both problem sizes. Even at the largest runs time-to-solution still largely decreases with increasing resources.</p><p>As usual with strong scaling, as more processors are used with the same-size problem, communication will eventually overwhelm useful computation and the simulation does not get any faster with more resources. The above figure shows that this point has not yet been reached at approximately 32K CPUs for neither of these two mesh sizes on this machine. The point of diminishing returns is determined by the scalability of the algorithm, its implementation, the problem size, the efficiency of the underlying runtime system, the hardware (e.g., the network interconnect), and their configuration.</p></section><section id="zalcg_strong_scaling2"><h2><a href="#zalcg_strong_scaling2">Strong scaling of computation – second series</a></h2><p>Approximately 10 months after the above data, the same benchmark series has been rerun on the same machine, using the same code computing the same problem (nelem=794M, npoin=133M) using the same software configuration. The results are depicted below.</p><div class="m-col-m-10 m-center-m"><img class="m-image" src="images/xyst_zalcg_strong2.png" alt="Image" /></div><p>This figure shows that though strong scaling is not ideal, using larger number of CPUs still significantly improves runtimes up to and including the largest run employing 65,536 CPU cores. Considering the mesh with 794,029,446 tetrahedra connecting 133,375,577 points, this corresponds to lower than 3K mesh points per CPU core on average. Advancing a single (one-stage) time step takes about 20 milliseconds of wall-clock time on average with this mesh on 65K CPU cores.</p><aside class="m-note m-info"><h4>Note</h4><p>One practical result from the above figures is that as the number of computational elements (and mesh points) per CPU core decreases with more resources available for a fixed-size problem, this solver implementation (on this machine) is scalable at least as low as approximately 3K mesh points per CPU core. This can be used to estimate effective compute resources based on problem size.</p></aside><p>Comparing the 133Mpts series in the two figures also reveals that they differ around and above approximately 16K CPUs. We believe this may be due to different configurations of hardware, operating system, the network interconnect and/or it could also be due to different background loads between the two series.</p><aside class="m-note m-info"><h4>Note</h4><p>Comparing the same data on <a href="riecg_performance.html" class="m-doc">RieCG</a> shows that the <a href="inciter_zalcg.html" class="m-doc">ZalCG</a> solver is roughly 2x faster for the same problem size using the same resources.</p></aside></section>
      </div>
    </div>
  </div>
</article></main>
<div class="m-doc-search" id="search">
  <a href="#!" onclick="return hideSearch()"></a>
  <div class="m-container">
    <div class="m-row">
      <div class="m-col-m-8 m-push-m-2">
        <div class="m-doc-search-header m-text m-small">
          <div><span class="m-label m-default">Tab</span> / <span class="m-label m-default">T</span> to search, <span class="m-label m-default">Esc</span> to close</div>
          <div id="search-symbolcount">&hellip;</div>
        </div>
        <div class="m-doc-search-content">
          <form>
            <input type="search" name="q" id="search-input" placeholder="Loading &hellip;" disabled="disabled" autofocus="autofocus" autocomplete="off" spellcheck="false" />
          </form>
          <noscript class="m-text m-danger m-text-center">Unlike everything else in the docs, the search functionality <em>requires</em> JavaScript. Enable it or <a href="https://searx.be/search?q=site:xyst.cc+">use an external search engine</a>.</noscript>
          <div id="search-help" class="m-text m-dim m-text-center">
            <p class="m-noindent">Search for symbols, directories, files, pages or
            modules. You can omit any prefix from the symbol or file path; adding a
            <code>:</code> or <code>/</code> suffix lists all members of given symbol or
            directory.</p>
            <p class="m-noindent">Use <span class="m-label m-dim">&darr;</span>
            / <span class="m-label m-dim">&uarr;</span> to navigate through the list,
            <span class="m-label m-dim">Enter</span> to go.
            <span class="m-label m-dim">Tab</span> autocompletes common prefix, you can
            copy a link to the result using <span class="m-label m-dim">⌘</span>
            <span class="m-label m-dim">L</span> while <span class="m-label m-dim">⌘</span>
            <span class="m-label m-dim">M</span> produces a Markdown link.</p>
          </div>
          <div id="search-notfound" class="m-text m-warning m-text-center">Sorry, nothing was found.<br />Maybe try a full-text <a href="#" id="search-external" data-search-engine="https://searx.be/search?q=site:xyst.cc+{query}">search with external engine</a>?</div>
          <ul id="search-results"></ul>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="search-v2.js"></script>
<script src="searchdata-v2.js" async="async"></script>
<footer><nav>
  <div class="m-container">
    <div class="m-row">
      <div class="m-col-l-10 m-push-l-1">
        <p>Copyright © J. Bakosi 2012&ndash;2015, Los Alamos National Security, LLC, 2016&ndash;2018, Triad National Security, LLC 2019-2021, J. Bakosi 2022-2025. Generated on Monday, Feb 17, 2025 using <a href="http://doxygen.org/">doxygen</a> and <a href="http://mcss.mosra.cz/">m.css</a>. Contact us via <a href="https://matrix.to/#/#xyst:matrix.org">matrix</a>.</p>
      </div>
    </div>
  </div>
</nav></footer>
</body>
</html>
