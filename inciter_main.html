<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <title>Inciter | Xyst docs</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,600i%7CSource+Code+Pro:400,400i,600" />
  <link rel="stylesheet" href="xyst.m-dark-noindent+doxygen.compiled.css" />
  <link rel="icon" href="Eo_circle_deep-orange_letter-x.svg" type="image/svg+xml" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="theme-color" content="#22272e" />
</head>
<body>
<header><nav id="navigation">
  <div class="m-container">
    <div class="m-row">
      <a href="index.html" id="m-navbar-brand" class="m-col-t-8 m-col-m-none m-left-m">Xyst <span class="m-thin">docs</span></a>
      <div class="m-col-t-4 m-hide-m m-text-right m-nopadr">
        <a href="#search" class="m-doc-search-icon" title="Search" onclick="return showSearch()"><svg style="height: 0.9rem;" viewBox="0 0 16 16">
          <path id="m-doc-search-icon-path" d="m6 0c-3.31 0-6 2.69-6 6 0 3.31 2.69 6 6 6 1.49 0 2.85-0.541 3.89-1.44-0.0164 0.338 0.147 0.759 0.5 1.15l3.22 3.79c0.552 0.614 1.45 0.665 2 0.115 0.55-0.55 0.499-1.45-0.115-2l-3.79-3.22c-0.392-0.353-0.812-0.515-1.15-0.5 0.895-1.05 1.44-2.41 1.44-3.89 0-3.31-2.69-6-6-6zm0 1.56a4.44 4.44 0 0 1 4.44 4.44 4.44 4.44 0 0 1-4.44 4.44 4.44 4.44 0 0 1-4.44-4.44 4.44 4.44 0 0 1 4.44-4.44z"/>
        </svg></a>
        <a id="m-navbar-show" href="#navigation" title="Show navigation"></a>
        <a id="m-navbar-hide" href="#" title="Hide navigation"></a>
      </div>
      <div id="m-navbar-collapse" class="m-col-t-12 m-show-m m-col-m-none m-right-m">
        <div class="m-row">
          <ol class="m-col-t-12 m-col-m-none">
            <li>
              <a href="index.html#mainpage_tools">Tools</a>
              <ol>
                <li><a href="inciter_main.html" id="m-navbar-current">Inciter</a></li>
                <li><a href="meshconv_main.html">MeshConv</a></li>
                <li><a href="unittest_main.html">UnitTest</a></li>
              </ol>
            </li>
            <li>
              <a href="vnv.html">V&amp;V</a>
              <ol>
                <li><a href="vnv.html#riecg_vnv">RieCG</a></li>
                <li><a href="vnv.html#kozcg_vnv">KozCG</a></li>
                <li><a href="vnv.html#zalcg_vnv">ZalCG</a></li>
                <li><a href="vnv.html#laxcg_vnv">LaxCG</a></li>
                <li><a href="vnv.html#chocg_vnv">ChoCG</a></li>
                <li><a href="vnv.html#lohcg_vnv">LohCG</a></li>
              </ol>
            </li>
            <li><a href="howtos.html">Howtos</a></li>
            <li><a href="papers.html">Papers</a></li>
            <li>
              <a href="https://codeberg.org/xyst/xyst">Source</a>
              <ol>
                <li><a href="namespaces.html">Namespaces</a></li>
                <li><a href="annotated.html">Classes</a></li>
                <li><a href="files.html">Files</a></li>
                <li><a href="licenses.html">Libraries</a></li>
                <li><a href="coverage.html">Quality</a></li>
                <li><a href="contributing.html">Contributing</a></li>
              </ol>
            </li>
          </ol>
          <ol class="m-col-t-6 m-col-m-none" start="6">
            <li class="m-show-m"><a href="#search" class="m-doc-search-icon" title="Search" onclick="return showSearch()"><svg style="height: 0.9rem;" viewBox="0 0 16 16">
              <use href="#m-doc-search-icon-path" />
            </svg></a></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</nav></header>
<main><article>
  <div class="m-container m-container-inflatable">
    <div class="m-row">
      <div class="m-col-l-10 m-push-l-1">
        <h1>
          Inciter
        </h1>
<p><strong>Euler and Navier-Stokes solvers for engineering flows</strong></p><p>Inciter contains multiple flow solvers for complex 3D engineering geometries specialized to different families of problems. The software implementation facilitates effective use of any multi-CPU computer from a laptop to the largest distributed-memory machines, combining data-, and task-parallelism on top of the <a href="http://charmplusplus.org">Charm++</a> runtime system. Charm++&#x27;s execution model is asynchronous by default, allowing arbitrary overlap of computation and communication. Built-in automatic load balancing enables redistribution of arbitrarily heterogeneous computational load based on real-time CPU load measurement at negligible cost. The runtime system also features automatic checkpointing, fault tolerance, resilience against hardware failure, and supports power-, and energy-aware computation.</p><p>Computational domains of arbitrary shapes are discretized into tetrahedron elements and decomposed into small chunks assigned to different CPUs. The number of chunks may be more than the number of CPUs, allowing <em>overdecomposition</em>, useful for effective cache utilization and automatic load balancing. The solution along partition boundaries, that exists on multiple processing elements, is made consistent with <em>asynchronous</em> communication which hides latencies by enabling overlapping of computation and communication.</p><section id="inciter_solvers"><h2><a href="#inciter_solvers">Solvers</a></h2><p>Inciter contains the following solvers. The numerical methods all belong to the family of continuous Galerkin finite element methods storing solution values at nodes of the computational mesh. See below for more details on how the various solvers are specialized.</p><section id="inciter_solvers_riecg"><h3><a href="#inciter_solvers_riecg">RieCG: for accurate simulation of energetic, high-speed, compressible, inviscid flows</a></h3><p>Solves the Euler equations modeling highly compressible inviscid flows. Compared to <strong>KozCG</strong>, <strong>RieCG</strong> uses a super-edge-based implementation of the finite element operators to reduce indirect addressing and thus to increase performance. It also contains a Riemann solver with a configurable numerical flux and piecewise limited solution reconstruction to ensure second order numerical accuracy. <strong>RieCG</strong> uses explicit three-stage Runge-Kutta time integration. More details: <a href="inciter_riecg.html" class="m-doc">method</a>, <a href="vnv.html#riecg_vnv" class="m-doc">V&amp;V</a>, <a href="riecg_performance.html" class="m-doc">performance</a>.</p></section><section id="inciter_solvers_kozcg"><h3><a href="#inciter_solvers_kozcg">KozCG: a simple implementation of a finite element method for compressible flows</a></h3><p>Solves the Euler equations modeling highly compressible inviscid flows. Compared to <strong>RieCG</strong>, <strong>KozCG</strong> contains a simpler, element-based implementation of the continuous Galerkin finite element operators. This is combined with Taylor-Galerkin stabilization and explicit single-step Euler time marching. The solver employs flux-corrected transport to ensure oscillation-free solutions with second order numerical accuracy. Compared to <strong>RieCG</strong>, <strong>KozCG</strong> does not require a Riemann solver or explicit gradient computations. More details: <a href="inciter_kozcg.html" class="m-doc">method</a>, <a href="vnv.html#kozcg_vnv" class="m-doc">V&amp;V</a>.</p></section><section id="inciter_solvers_zalcg"><h3><a href="#inciter_solvers_zalcg">ZalCG: for fast simulation of energetic, high-speed, compressible, inviscid flows</a></h3><p>Solves the Euler equations modeling highly compressible inviscid flows. Similar to <strong>RieCG</strong>, <strong>ZalCG</strong> also employs a super-edge-based implementation of the finite element operators to reduce indirect addressing compared to element-based loops, but uses the cheaper Taylor-Galerkin numerical flux to stabilize advection. This is combined with flux-corrected transport (implemented over edges, compared to the element-based loops in <strong>KozCG</strong>) to avoid unphysical numerical oscillations. Since this solver does not require Riemann solvers or gradient computations it is generally faster than <strong>RieCG</strong> and <strong>KozCG</strong>. <strong>ZalCG</strong> can further save CPU time by dynamically deactivating inactive partitions of the computational mesh during parallel flow simulations of propagation phenomena, e.g., detonations or scalar transport. The parallel imbalance is then homogenized by Charm++&#x27;s built-in load balancers. More details: <a href="inciter_zalcg.html" class="m-doc">method</a>, <a href="vnv.html#zalcg_vnv" class="m-doc">V&amp;V</a>, <a href="zalcg_performance.html" class="m-doc">performance</a>.</p></section><section id="inciter_solvers_laxcg"><h3><a href="#inciter_solvers_laxcg">LaxCG: for energetic, high-speed, compressible, inviscid flows at all Mach numbers</a></h3><p>Solves the Euler equations modeling compressible inviscid flows. The solver in <strong>LaxCG</strong> is very similar to <strong>RieCG</strong>, but <strong>LaxCG</strong> applies a time-derivative preconditioning technique to enable computation of flows and flow regions at all Mach numbers. More details: <a href="inciter_laxcg.html" class="m-doc">method</a>, <a href="vnv.html#laxcg_vnv" class="m-doc">V&amp;V</a>.</p></section><section id="inciter_solvers_chocg"><h3><a href="#inciter_solvers_chocg">ChoCG: for accurate simulation of constant-density, inviscid or viscous flows</a></h3><p>Solving the Navier-Stokes equation modeling constant-density viscous flows, <strong>ChoCG</strong> enforces the divergence-free constraint by solving a Poisson equation for the pressure increment in time and projecting the velocity field to a divergence-free subspace. The advection and divergence operators, resulting from the continuous Galerkin finite element discretization, are stabilized by edge-based consistent numerical fluxes. More details: <a href="inciter_chocg.html" class="m-doc">method</a>, <a href="vnv.html#chocg_vnv" class="m-doc">V&amp;V</a>, <a href="chocg_performance.html" class="m-doc">performance</a>.</p></section><section id="inciter_solvers_lohcg"><h3><a href="#inciter_solvers_lohcg">LohCG: for fast simulation of constant-density, inviscid or viscous flows</a></h3><p>Solving the Navier-Stokes equation modeling constant-density viscous flows, <strong>LohCG</strong> is similar to <strong>ChoCG</strong>, but enforces the divergence-free constraint by recasting the elliptic problem of the Poisson equation to a hyperbolic/parabolic system using an artificial compressibility technique. The method is stabilized by artificial viscosity. This yields a numerical method that can be 100x faster than <strong>ChoCG</strong> and also scales better to larger problems and larger resources. More details: <a href="inciter_lohcg.html" class="m-doc">method</a>, <a href="vnv.html#lohcg_vnv" class="m-doc">V&amp;V</a>, <a href="lohcg_performance.html" class="m-doc">performance</a>.</p></section></section><section id="inciter_pages"><h2><a href="#inciter_pages">Related pages</a></h2><ul><li><a href="howtos.html" class="m-doc">Howto pages</a></li><li><a href="papers.html" class="m-doc">Publications</a></li><li><a href="inciter_design.html" class="m-doc">Software design</a></li></ul></section>
      </div>
    </div>
  </div>
</article></main>
<div class="m-doc-search" id="search">
  <a href="#!" onclick="return hideSearch()"></a>
  <div class="m-container">
    <div class="m-row">
      <div class="m-col-m-8 m-push-m-2">
        <div class="m-doc-search-header m-text m-small">
          <div><span class="m-label m-default">Tab</span> / <span class="m-label m-default">T</span> to search, <span class="m-label m-default">Esc</span> to close</div>
          <div id="search-symbolcount">&hellip;</div>
        </div>
        <div class="m-doc-search-content">
          <form>
            <input type="search" name="q" id="search-input" placeholder="Loading &hellip;" disabled="disabled" autofocus="autofocus" autocomplete="off" spellcheck="false" />
          </form>
          <noscript class="m-text m-danger m-text-center">Unlike everything else in the docs, the search functionality <em>requires</em> JavaScript. Enable it or <a href="https://searx.be/search?q=site:xyst.cc+">use an external search engine</a>.</noscript>
          <div id="search-help" class="m-text m-dim m-text-center">
            <p class="m-noindent">Search for symbols, directories, files, pages or
            modules. You can omit any prefix from the symbol or file path; adding a
            <code>:</code> or <code>/</code> suffix lists all members of given symbol or
            directory.</p>
            <p class="m-noindent">Use <span class="m-label m-dim">&darr;</span>
            / <span class="m-label m-dim">&uarr;</span> to navigate through the list,
            <span class="m-label m-dim">Enter</span> to go.
            <span class="m-label m-dim">Tab</span> autocompletes common prefix, you can
            copy a link to the result using <span class="m-label m-dim">⌘</span>
            <span class="m-label m-dim">L</span> while <span class="m-label m-dim">⌘</span>
            <span class="m-label m-dim">M</span> produces a Markdown link.</p>
          </div>
          <div id="search-notfound" class="m-text m-warning m-text-center">Sorry, nothing was found.<br />Maybe try a full-text <a href="#" id="search-external" data-search-engine="https://searx.be/search?q=site:xyst.cc+{query}">search with external engine</a>?</div>
          <ul id="search-results"></ul>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="search-v2.js"></script>
<script src="searchdata-v2.js" async="async"></script>
<footer><nav>
  <div class="m-container">
    <div class="m-row">
      <div class="m-col-l-10 m-push-l-1">
        <p>Copyright © J. Bakosi 2012&ndash;2015, Los Alamos National Security, LLC, 2016&ndash;2018, Triad National Security, LLC 2019-2021, J. Bakosi 2022-2025. Generated on Monday, Feb 17, 2025 using <a href="http://doxygen.org/">doxygen</a> and <a href="http://mcss.mosra.cz/">m.css</a>. Contact us via <a href="https://matrix.to/#/#xyst:matrix.org">matrix</a>.</p>
      </div>
    </div>
  </div>
</nav></footer>
</body>
</html>
